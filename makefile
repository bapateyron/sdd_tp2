FLAGS := -Wall -Wextra -ansi -pedantic -g

all: prog.bin

prog.o: main.c truc.c tests.c piles.c files.c
	gcc $(FLAGS) -c *.c

prog.bin: truc.o tests.o files.o piles.o main.o
	gcc $(FLAGS) truc.o tests.o files.o piles.o main.o -o prog.exe

clean:
	rm *.o *~ vgcore* *.exe
