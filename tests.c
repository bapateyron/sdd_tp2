/*******************************************************************************
* 															TESTS.C
*
* Implementation de toutes les fonctions de test qui pour chacune d'elles teste
* pour une fonction du programme en particulier, l'ensemble des cas inherents
* a cette fonction.
*
*******************************************************************************/

#include "tests.h"

/* Tests TRUC *****************************************************************/
void test_recursifTruc()
{
	printf("\n\n\n--- test_recursifTruc(): recursifTruc(0, 3, \"ABC\") -----------\n");
	printf("(Car il s'agit d'un tableau en C donc de premier indice = 0)\n");
	recursifTruc(0, 3, "ABC");
}

void test_iteratifTruc()
{
	/* On realise toutes les permutations possibles a partir du premier indice */
	/* C'est a dire l'indice 0 de notre chaine (ce qui correspond a l'indice 1 sur le sujet du TP) */
	printf("\n\n\n--- test_iteratifTruc(): iteratifTruc(0, 3, \"ABC\") -----------\n");
	iteratifTruc(0, 3, "ABC");
}

/* Tests PILES ****************************************************************/

void test_initialiserPile()
{
	int 			code		= 0;
	int 			taille	= 10;
	pile_t *	pile		= NULL;

	puts("\n\n\n--- test_initialiserPile(): Initialisation d'une pile de taille 0 (Echec)\n");
	pile	= initialiserPile(0);
	afficherPile(pile);

	puts("\n\n\n--- test_initialiserPile(): Initialisation d'une pile correcte ----\n");
	pile	= initialiserPile(taille);
	printf("Taille: %d\tSommet: %d\tBase: %p\n", pile->taille, pile->sommet, pile->base);
	afficherPile(pile);

	libererPile(pile);
}

void test_empiler()
{
	int 			code		= 0;
	int 			taille	= 5;
	val_t 		v				= 0;
	pile_t	*	pile		= NULL;

	printf("\n\n\n--- test_empiler(): Cas Pile NULL -----------------------------\n");
	empiler(pile, v, &code);
	afficherPile(pile);

	pile	=	initialiserPile(taille);

	printf("\n\n\n--- test_empiler(): Empilage de (%d) valeurs dans une pile de taille (%d)\n", taille, taille);
	for(v; v < taille + 1; v++)
	{
		if(v == taille)
		{
			printf("\n\n\n--- test_empiler():\tLa pile (de taille %d) est maintenant pleine\n---\t\t\tOn aura une erreur lors de l'empilement\n", taille);
		}
		printf("\n/* On insert la valeur (%d) dans la pile */\n", v);
		empiler(pile, v, &code);
		printf("CodeErreur?: %d", code);
		afficherPile(pile);
	}

	libererPile(pile);
}

void test_depiler()
{
	int				code		= 0;
	int				cas			= 0;	/* Permet de tester d'autres cas particuliers */
	int				taille	= 5;
	int				i				= 0;
	val_t			valeur	= 0;
	pile_t	* pile		= initialiserPile(taille);

	puts("\n\n\n--- test_depiler(): Cas pile NULL --------------------------------\n");
	depiler(NULL, &valeur, &code);
	printf("CodeErreur?: %d", code);

	puts("\n\n--- test_depiler(): Depilage complet de la pile ------------------\n");

	/* Creation d'une pile de test */
	for(i; i < taille; i++)	empiler(pile, i, &code);
	printf("/* Pile test cree */");
	afficherPile(pile);

	while(pile->sommet != -1 || cas != 2)
	{

		if(cas == 1)
		{
			puts("\n\n\n--- test_depiler(): Cas apres nouvel empilage --------------------\n");
			printf("/* On empile la valeur (7) */");
			empiler(pile, 7, &code);
			afficherPile(pile);
			printf("\n/* Puis on depile */");
			cas = 2;
		}

		if(pile->sommet == -1 && cas == 0)	/* Pour tester le depilage sur liste vide */
		{
			printf("\n\n\n--- test_depiler():\tLa pile (de taille %d) est maintenant vide\n---\t\t\tOn aura une erreur lors du prochain depilement\n", taille);
			cas	= 1;	/* On change la seconde condition pour passer au cas suivant */
		}

		depiler(pile, &valeur, &code);
		if(!code) printf("\nValeur depilee: (%d)", valeur);
		printf("\nCodeErreur?: %d", code);
		afficherPile(pile);
	}

	libererPile(pile);
}

/* Tests FILES ****************************************************************/
void test_initialiserFile()
{
	int 			code		= 0;
	int 			taille	= 10;
	file_t *	file		= NULL;

	puts("\n\n\n--- test_initialiserFile(): Initialisation d'une file de taille 0 (Echec) ---\n");
	file	= initialiserFile(0);
	afficherFile(file);

	puts("\n\n\n--- test_initialiserFile(): Initialisation d'une file correcte ----\n");
	file	= initialiserFile(taille);
	printf("Taille: %d\tNombreElements: %d\tDebut: %d\tFin: %d\tBase: %p\n", file->taille, file->nombreElement, file->debut, file->fin, file->base);
	afficherFile(file);

	libererFile(file);
}

void test_enfiler()
{
	int				code		= 0;
	int				taille	= 5;
	int				v				= 0;
	file_t	* file		= initialiserFile(taille);

	puts("\n\n\n--- test_enfiler(): Cas file NULL --------------------------------\n");
	enfiler(NULL, 17, &code);
	afficherFile(NULL);

	printf("\n\n\n--- test_enfiler(): Cas enfilages successifs --------------------\n");
	afficherFile(file);

	for(v; v < taille + 1; v++)
	{
		if(v == taille)
		{
			printf("\n\n\n--- test_enfiler(): La file (de taille %d) est maintenant pleine\nOn aura une erreur lors de l'enfilement\n", taille);
		}
		printf("\n/* On insert la valeur (%d) dans la file */\n", v);
		enfiler(file, v, &code);
		printf("CodeErreur?: %d", code);
		afficherFile(file);
	}
	printf("\n\n\n--- test_enfiler(): Cas ou on enfile apres avoir defile --------\n");
	printf("\n/* On defile 2 fois */\n");
	defiler(file, &v, &code);
	defiler(file, &v, &code);
	afficherFile(file);

	printf("\n/* Enfile la valeur (7) */\n");
	enfiler(file, 7, &code);
	afficherFile(file);

	libererFile(file);
}

void test_defiler()
{
	int				code		= 0;
	int				taille	= 5;
	int				i				= 0;
	val_t			valeur	= 0;
	file_t	* file		= initialiserFile(taille);

	puts("\n\n\n--- test_defiler(): Cas file NULL --------------------------------\n");
	defiler(NULL, &valeur, &code);
	printf("CodeErreur?: %d", code);

	puts("\n\n--- test_defiler(): Defilage complet de la file --------------------\n");

	/* Creation d'une file de test */
	for(i; i < taille; i++)	enfiler(file, i, &code);
	printf("/* File test cree */");
	afficherFile(file);

	/* Defilage des valeurs + 1 (pour tester le cas liste vide) */
	for(i = file->debut; i < taille + 1; i++)
	{
		if(i == taille)	/* Pour tester le defilage sur liste vide */
		{
			printf("\n\n\n--- test_defiler():\tLa file (de taille %d) est maintenant vide\n---\t\t\tOn aura une erreur lors du prochain defilement\n", taille);
		}

		defiler(file, &valeur, &code);
		if(!code)	printf("\nValeur defilee: (%d)\n", valeur);
		printf("CodeErreur?: %d\n", code);
		afficherFile(file);
	}

	libererFile(file);
}
