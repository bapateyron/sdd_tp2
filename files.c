/*******************************************************************************
 * 																	FILES.C
 *
 * Implementation des fonctions du fichier files.h qui permettent de realiser
 * les operations de manipulation elementaires des files
 *
 ******************************************************************************/

#include "files.h"

/*******************************************************************************
 * Alloue puis initialise une file de 'taille' elements, a condition que la
 * taille soit strictement positive
 *
 * @param 	taille:	Taille totale du nombre de cellules pour la file
 * @return 					Adresse de la file cree
 ******************************************************************************/
file_t	*	initialiserFile(int taille)
{
	file_t * file = NULL;

	if(taille > 0)
	{
		file	= (file_t *) malloc (sizeof(file_t));

		if(file == NULL)
		{
			fprintf(stderr, "initialiserFile(): Allocation file impossible");
			file	= NULL;
		}
		else
		{
			file->base		= (val_t *) malloc (sizeof(val_t) * taille);

			if(file->base == NULL)
			{
				fprintf(stderr, "initialiserFile(): Allocation base impossible");
				free(file);
				file	= NULL;
			}
			else
			{
				file->taille				= taille;
				file->nombreElement	= 0;
				file->debut					= 0;
				file->fin						= taille - 1;
			}
		}
	}
	else
	{
		printf("initialiserFile(): Une taille de file (%d) est < 1 et n'est pas valide	-> File non cree\n", taille);
	}
	return file;
}

/*******************************************************************************
 * On decale l'indice de FIN de file puis y insert la valeur 'v'
 * L'insertion se fait a condition que:
 *	- La file existe
 *	- Il reste une place libre pour inserer
 *
 * @param	file:	Adresse de la file cible
 * @param	v:		Valeur a inserer
 * @param	code:	Adresse pour renseigner un succes (0) ou un echec (1)
 ******************************************************************************/
void enfiler(file_t * file, val_t v, int * code)
{
	*code	= 1;																										/* Mis en erreur par defaut */

	if(file)
	{
		if (file->nombreElement < file->taille) 										/* S il reste de la place */
		{
			file->fin							= (file->fin + 1) % (file->taille);	/* On decale la fin */
			file->base[file->fin]	= v;																/* On enfile la valeur */
			*code									= 0;																/* Code = SUCCES */
			file->nombreElement++;																		/* On incremente le nombre d'elements */
		}
		else
		{
			fprintf(stderr, "enfiler(): File pleine, enfilage impossible\n");
		}
	}
	else
	{
		fprintf(stderr, "enfiler(): File inexistante, impossible d'enfiler\n");
	}
}

/*******************************************************************************
 * Affecte a l'adresse 'v' la valeur a l'indice de DEBUT puis decale ce dernier
 * L'operation a lieu a condition que:
 *	- La file existe
 *	- La file ne soit pas pleine
 *
 * @param file:	Adresse de la file cible
 * @param	v:		Adresse dans laquelle placer la valeur defilee
 * @param	code:	Adresse pour renseigner un succes (0) ou un echec (1)
 ******************************************************************************/
void	defiler(file_t * file, val_t * v, int * code)
{
	*code	= 1;																						/* Mis en erreur par defaut */

	if(file)
	{
		if (file->nombreElement)														/* S il reste un element */
		{
			*v					= file->base[file->debut];						/* On met la valeur de debut dans v */
			file->debut	= (file->debut + 1) % (file->taille);	/* On decale le debut */
			*code				= 0;																	/* Code = SUCCES */
			file->nombreElement--;														/* On decremente le nombre d'elements */
		}
		else
		{
			fprintf(stderr, "defiler(): File vide, impossible de defiler\n");
		}
	}
	else
	{
		fprintf(stderr, "defiler(): File inexistante, impossible de defiler\n");
	}
}

/*******************************************************************************
 * Liberation memoire de la 'base' donc des elements de la file
 * Puis dans un second temps, liberation de la structure de la file elle-meme
 *
 * @param file:	Adresse de la file a liberer
 ******************************************************************************/
void	libererFile(file_t * file)
{
	if (file != NULL)
	{
		if (file->base != NULL)
		{
			free(file->base);
		}

		free(file);
	}
}

/*******************************************************************************
 * Affiche les elements de la file en respectant l'ordre (FIFO) a condition que:
 *	- La file existe
 *	- La file ne soit pas vide
 *
 * @param file:	Adresse de la file a afficher
 ******************************************************************************/
void afficherFile(file_t * file)
{
	int i	= 0;	/* Indice de parcours de la file */

	if(file)
	{
		if(file->nombreElement == 0)
		{
			puts("\n[ File vide ]");
		}
		else
		{
			printf("\n[ ");
			for(i; i < file->nombreElement; i++)
			{
				printf("%d ", file->base[ (file->debut + i) % file->taille]);
			}

			printf("]\n");
		}
	}
	else
	{
		fprintf(stderr, "afficherFile(): File non allouee\n");
	}
}
