/*******************************************************************************
* 															TESTS.H
*
* Fichier en-tete qui declare les prototypes de toutes les fonctions de test
* des modules de Piles, Files et Truc qui seront appelees dans le main
*
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "truc.h"
#include "files.h"
#include "piles.h"


/* Tests TRUC *****************************************************************/
void test_recursifTruc();
void test_iteratifTruc();

/* Tests PILE *****************************************************************/
void test_initialiserPile();
void test_empiler();
void test_depiler();

/* Tests FILE *****************************************************************/
void test_initialiserFile();
void test_enfiler();
void test_defiler();
