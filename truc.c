/*******************************************************************************
* 															TRUC.C
*
* Implementation de la fonction TRUC en recursif puis en iteratif ainsi que de
* la fonction d'echange utilisee.
*
*******************************************************************************/

#include "truc.h"
#include "files.h"
#include "piles.h"

/*******************************************************************************
* Donne toutes les permutations possibles de la liste 's' en version recursive
*
* @param: i: Indice du premier element a inverser
* @param: n: Le nombre d'element de la liste a permurter
* @param: s: Une liste de carateres d'indice 0
*******************************************************************************/
void recursifTruc(int i, int n,char s[])
{

	int 	j;											/* Indice de parcourt */
	char 	T[100];									/* Copie locale de 's' pour les permutations */

	strcpy(T, s);									/* On copie le contenu de la liste dans T */

	if (i == n)
	{
		for (j = 0; j < n; j++)
		{
			printf(" %c ", T[j]);
		}
		printf(" \n");
	}
	else
	{
		for (j = i; j < n; j++)
		{
			echanger(&T[i], &T[j]);			/* On inverse le contenu dans le tableau aux positions i et j */
			recursifTruc(i + 1, n, T); 	/* Appel recursif */
			echanger(&T[i], &T[j]); 		/* On inverse le contenu dans le tableau aux positions i et j */
		}
	}
}

/*******************************************************************************
* Donne toutes les permutations possibles de la liste s en version iterative
*
* @param	i:	indice du premier element a inverser
* @param	n:	Le nombre d'element de la liste a permurte
* @param	s:	Une liste de carateres d'indice 0
*******************************************************************************/
void iteratifTruc(int i, int n,char s[])
{
	int 			j	  	= i; 										/* Variable de parcourt */
	int 			fin 	= 0;
	int 			code	= 0;										/* code est le code erreur pour la piles */
	pile_t *  p0		= initialiserPile(100);	/* p0 est un pointeur de pile */
	char 			T[100];												/* Copie locale de s pour faire les permutations */

	strcpy(T, s);														/* On copie le contenu de la liste dans T */

	if (p0)																	/* On verifie que l'allocation a reussie */
	{
		while (fin == 0)
		{
			if (j < n)
			{
				echanger(&T[i], &T[j]); 					/* On inverse le contenu dans le tableau aux positions i et j */
				empiler(p0, j, &code);		 				/* On empile j */

				i	= i + 1;
				j	= i;
			}
			else
			{
				if (i == n - 1)  									/* On affiche la liste quand i vaut n-1 car ici c'est un tableau d'indice 0 */
				{																	/* Alors que sur le sujet il s'agit d'un tableau d'indice 1 */
					for (j = 0; j < n; j++)
					{
						printf(" %c ",T[j]);
					}
					printf(" \n");
				}
				if (!estPileVide(p0))  						/* On regarde si la pile est non vide */
				{
					depiler(p0, &j, &code);					/* On depile J */
					i -= 1;
					echanger(&T[i], &T[j]);					/* On inverse le contenu dans le tableau aux positions i et j */
					j += 1;
				}
				else
				{
					fin = 1;												/* Arret de la boucle */
				}
			}
		}
	}
	else  																	/* Pile non allouee */
	{
		printf("iteratifTruc(): Echec de l'allocation de la pile\n");
	}

	libererPile(p0);												/* On libere la pile */
}

/*******************************************************************************
* Inverse les contenus de a et b
*
* @param: a: Adresse d'un char
* @param: b: Adresse d'un char
*******************************************************************************/
void echanger(char * a, char * b)
{
	char tmp 	= *a;		/* Variable temporaire d'echange */
	*a				= *b;
	*b				= tmp;
}
