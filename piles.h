/*******************************************************************************
* 															PILES.H
*
* Fichier en-tete qui declare les prototypes de toutes les fonctions
* gerant la manipulation des piles
*
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Type generique a adapter en fonction du type des valeurs dans la pile */
#ifndef VALT
	typedef int val_t;
	#define VALT
#endif

/* Structure de la pile */
typedef struct pile_t
{
	int			taille;			/* Nombre d'elements */
	int			sommet;			/* Indice du dernier element ajoute */
	val_t	*	base;				/* Tableau des elements pointant sur le premier element ajoute */
} pile_t;

/* Prototypes *****************************************************************/
pile_t	*	initialiserPile(int taille);
int				estPileVide(pile_t * pile);
void			empiler(pile_t * pile, val_t v, int * code);
void			depiler(pile_t * pile, val_t * v, int * code);
void			libererPile(pile_t * pile);
void			afficherPile( pile_t * pile);
