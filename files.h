/*******************************************************************************
* 															FILES.H
*
* Fichier en-tete qui declare les prototypes de toutes les fonctions
* gerant la manipulation des files
*
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Type generique a adapter en fonction du type des valeurs dans la pile */
#ifndef VALT
	typedef int val_t;
	#define VALT
#endif

/* Structure de la file */
typedef struct file_t
{
	int			taille;					/* Nombre d'elements */
	int			nombreElement;	/* Nombre d'elements presents dans la file */
	int			debut;					/* Indice du prochain element a servir */
	int 		fin;						/* Indice du prochain element a enfiler */
	val_t	*	base;						/* Adresse de la liste */
} file_t;

/* Prototypes *****************************************************************/
file_t	*	initialiserFile(int taille);
void			enfiler(file_t * file, val_t v, int * code);
void			defiler(file_t * file, val_t * v, int * code);
void			libererFile(file_t * file);
void			afficherFile(file_t * file);
