#include "tests.h"

int main()
{
	/*** Piles ***/
	test_initialiserPile();
	test_empiler();
	test_depiler();

	/*** Files ***/
	test_initialiserFile();
	test_enfiler();
	test_defiler();

	/*** Truc ***/
	test_recursifTruc();
	test_iteratifTruc();

	printf("\n\n------ Fin du programme ------------------------------------------\n\n");

	return 0;
}
