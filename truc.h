/*******************************************************************************
* 															TRUC.H
*
* Fichier en-tete qui declare les prototypes de toutes des 3 fonctions utilisees
* dans le fichier 'truc.c'
*
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Prototypes *****************************************************************/
void recursifTruc(int i, int n, char s[]);
void iteratifTruc(int i, int n, char s[]);
void echanger(char * a, char * b);
